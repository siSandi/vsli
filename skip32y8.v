module adder(s, co, a, b, ci);

   output s, co;
   input  a, b, ci;
   
   wire   o0, o1, o2;
   
   xor(s, a, b, ci);
   
   or(o0, a, b);
   or(o1, b, ci);
   or(o2, ci, a);
   and(co, o0, o1, o2);
   
endmodule // adder


module adder8(s, co, a, b, ci);
   
   output [7:0] s;
   output 	co;
   input [7:0] a, b;
   input 	ci;
   
   wire 	c1, c2, c3, c4, c5, c6, c7;
   
   adder a0(s[0], c1, a[0], b[0], ci);
   adder a1(s[1], c2, a[1], b[1], c1);
   adder a2(s[2], c3, a[2], b[2], c2);
   adder a3(s[3], c4, a[3], b[3], c3);
   adder a4(s[4], c5, a[4], b[4], c4);
   adder a5(s[5], c6, a[5], b[5], c5);
   adder a6(s[6], c7, a[6], b[6], c6);
   adder a7(s[7], co, a[7], b[7], c7);
   
      
endmodule // adder8


module mux(f, ci, cr, p);

	//ci is the carry in bit
	//cr is the final ripple carry of the n bit block
	//p is the select signal calculated from the progagate signals of each operand bit of n size
	//Output f

	output f;
	input ci, cr, p;

	wire   f1, f2;

	and  g1(f1, cr, ~p);
	and  g2(f2, ci, p); 
	or   g3(f, f1, f2);

endmodule


module pblock(p,a,b);

	//a operand of n bits
	//b operand of n bits
	//output p=block propagation signal

	output p;
	input [0:7] a, b;
	
	wire p0, p1, p2, p3, p4, p5, p6, p7;

	//^ is XOR function
	assign p0=a[0]^b[0];
	assign p1=a[1]^b[1];
	assign p2=a[2]^b[2];
	assign p3=a[3]^b[3];
	assign p4=a[4]^b[4];
	assign p5=a[5]^b[5];
	assign p6=a[6]^b[6];
	assign p7=a[7]^b[7];

	assign p = p0 & p1 & p2 & p3 & p4 & p5 & p6 & p7;

endmodule


module skip32by8(s, co, a, b, ci);

	output [31:0] s;
	output co;

	input [31:0] a, b;
	input ci;

	wire c8, c16, c24;
	
	//Calculate propagate block signals
	pblock l1(p7, a[7:0], b[7:0]);
	pblock l2(p15, a[15:8], b[15:8]);	
	pblock l3(p23, a[23:16], b[23:16]);
	pblock l4(p31, a[31:24], b[31:24]);

	//Block 1
	//Detemine if skip is possible
	//c4r is the carry from the ripple addition
	mux m1(c8, ci, c8r, p7);
	adder8 a7(s[7:0], c8r, a[7:0], b[7:0], ci);
	  	
	//Block 2
	mux m2(c16, c8, c16r, p15);
	adder8 a15(s[15:8], c16r, a[15:8], b[15:8], c8);

	//Block 5	
	mux m3(c24, c16, c24r, p23);
	adder8 a23(s[23:16], c24r, a[23:16], b[23:16], c16);

	//Block 8	
	mux m4(co, c24, cor, p31);
	adder8 a31(s[31:24], cor, a[31:24], b[31:24], c24);

	

endmodule