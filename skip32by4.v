module mux(f, ci, cr, p);

	//ci is the carry in bit
	//cr is the final ripple carry of the n bit block
	//p is the select signal calculated from the progagate signals of each operand bit of n size
	//Output f

	output f;
	input ci, cr, p;

	wire   f1, f2;

	and  g1(f1, cr, ~p);
	and  g2(f2, ci, p); 
	or   g3(f, f1, f2);

endmodule


module pblock(p,a,b);

	//a operand of n bits
	//b operand of n bits
	//output p=block propagation signal

	output p;
	input [0:3] a, b;
	
	wire p0, p1, p2, p3;

	//^ is XOR function
	assign p0=a[0]^b[0];
	assign p1=a[1]^b[1];
	assign p2=a[2]^b[2];
	assign p3=a[3]^b[3];
	
	assign p = p0 & p1 & p2 & p3;

endmodule


module adder(s, co, a, b, ci);

   output s, co;
   input  a, b, ci;
   
   wire   o0, o1, o2;
   
   xor(s, a, b, ci);
   
   or(o0, a, b);
   or(o1, b, ci);
   or(o2, ci, a);
   and(co, o0, o1, o2);
   
endmodule 


module adder4(S, CO, A, B, CI);
	
	input [3:0] A,B;
	input CI;
	output [3:0] S;
	output CO;

	wire c0, c1, c2;

	adder fa0(S[0],c0,A[0],B[0],CI);
	adder fa1(S[1],c1,A[1],B[1],c0);
	adder fa2(S[2],c2,A[2],B[2],c1);
	adder fa3(S[3],CO,A[3],B[3],c2);
endmodule


module skip32by4(s, co, a, b, ci);

	output [31:0] s;
	output co;

	input [31:0] a, b;
	input ci;

	wire c4, c8, c12, c16, c20, c24, c28;
	
//Calculate propagate block signals
	pblock l1(p3, a[3:0], b[3:0]);
	pblock l2(p7, a[7:4], b[7:4]);	
	pblock l3(p11, a[11:8], b[11:8]);
	pblock l4(p15, a[15:12], b[15:12]);
	pblock l5(p19, a[19:16], b[19:16]);
	pblock l6(p23, a[23:20], b[23:20]);
	pblock l7(p27, a[27:24], b[27:24]);
	pblock l8(p31, a[31:28], b[31:28]);

	//Block 1
	//Detemine if skip is possible
	//c4r is the carry from the ripple addition
	mux m1(c4, ci, c4r, p3);
	adder4 a3(s[3:0], c4r, a[3:0], b[3:0], ci);
  	
	//Block 2
	mux m2(c8, c4, c8r, p7);
	adder4 a7(s[7:4], c8r, a[7:4], b[7:4], c4);

	//Block 3
	mux m3(c12, c8, c12r, p11);
	adder4 a11(s[11:8], c12r, a[11:8], b[11:8], c8);

	//Block 4
	mux m4(c16, c12, c16r, p15);
	adder4 a15(s[15:12], c16r, a[15:12], b[15:12], c12);

	//Block 5	
	mux m5(c20, c16, c20r, p19);
	adder4 a19(s[19:16], c20r, a[19:16], b[19:16], c16);
	
	//Block 6	
	mux m6(c24, c20, c24r, p23);
	adder4 a23(s[23:20], c24r, a[23:20], b[23:20], c20);

	//Block 7	
	mux m7(c28, c24, c28r, p27);
	adder4 a27(s[27:24], c28r, a[27:24], b[27:24], c24);

	//Block 8	
	mux m8(co, c28, cor, p31);
	adder4 a31(s[31:28], cor, a[31:28], b[31:28], c28);

	

endmodule