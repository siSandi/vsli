module adder(s, co, a, b, ci);

   output s, co;
   input  a, b, ci;
   
   wire   o0, o1, o2;
   
   xor(s, a, b, ci);
   
   or(o0, a, b);
   or(o1, b, ci);
   or(o2, ci, a);
   and(co, o0, o1, o2);
   
endmodule // adder


module adderA(s, co, a, b, si, ci);
   
   output s;
   output co;
   input a, b, si;
   input ci;
   
   wire	  ab;
   wire   o0, o1, o2;
	
   and(ab, a, b);

   xor(s, ab, si, ci);
   
   or(o0, ab, si);
   or(o1, ab, ci);
   or(o2, ci, si);
   and(co, o0, o1, o2);	
   
endmodule // adderA


module multi4(s, a, b);

	output [7:0] s;
	input [3:0] a, b;

	wire zero, one;
	wire [4:0] w0,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14,w15;
	wire [4:0] v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15;
	wire [4:0] c;

	assign zero = 0;
	
	
	adderA a0 (s[0]  , v0[0] , a[0] , b[0], zero, zero);
	adderA a1 (w0[0] , v0[1] , a[1] , b[0], zero, zero);
	adderA a2 (w0[1] , v0[2] , a[2] , b[0], zero, zero);
	adderA a3 (w0[2] , v0[3] , a[3] , b[0], zero, zero);

	adderA b0 (s[1]  , v1[0] , a[0] , b[1], w0[0] , v0[0]);
	adderA b1 (w1[0] , v1[1] , a[1] , b[1], w0[1] , v0[1]);
	adderA b2 (w1[1] , v1[2] , a[2] , b[1], w0[2] , v0[2]);
	adderA b3 (w1[2] , v1[3] , a[3] , b[1], zero , v0[3]);

	adderA c0 (s[2]  , v2[0] , a[0] , b[2], w1[0] , v1[0]);
	adderA c1 (w2[0] , v2[1] , a[1] , b[2], w1[1] , v1[1]);
	adderA c2 (w2[1] , v2[2] , a[2] , b[2], w1[2] , v1[2]);
	adderA c3 (w2[2] , v2[3] , a[3] , b[2], zero , v1[3]); 

	adderA d0 (s[3]  , v3[0] , a[0] , b[3], w2[0] , v2[0]);
	adderA d1 (w3[0] , v3[1] , a[1] , b[3], w2[1] , v2[1]);
	adderA d2 (w3[1] , v3[2] , a[2] , b[3], w2[2] , v2[2]);
	adderA d3 (w3[2] , v3[3] , a[3] , b[3], zero , v2[3]);

	adder q0 (s[4]  , c[0], w3[0], v3[0], zero);
	adder q1 (s[5]  , c[1], w3[1], v3[1], c[0]);
	adder q2 (s[6]  , c[2], w3[2], v3[2], c[1]);
	adder q3 (s[7]  , c[3], zero , v3[3], c[2]);

endmodule //multi4
